##Script to calculate the summary statistics of the microarray SNPs
##Author: Consuelo Quinto, Krishna Veeramah
##Jan 31, 2017

import sys
import subprocess
from subprocess import Popen
import os
##import dircache
import string
from string import join
import math
##from time import strftime
import random
from random import randint
from random import uniform
from random import gauss
from random import gammavariate
from random import betavariate
from math import sqrt
from sys import argv
import numpy as np
np.set_printoptions(threshold=np.nan)
import datetime
import gzip
from bisect import bisect_left
from bisect import bisect_right
import numpy as np
import re

###summary statistics####################################

##Function to calculate nucleotide diversity
def Pi2(spec,n): #standard pi, n = sample size (in chromosomes) and spec is the site frequency spectrum
	theta_pi=0.0

	for g in range(len(spec)):
		theta_pi=theta_pi+(2.0*float(spec[g])*(g+1.0)*(n-(g+1.0)))/(n*(n-1.0))

	return theta_pi

##Function to calculate FST on pi within populations and between populations
def FST2(seq1,pi1,nseq1,seq2,pi2,nseq2):
	## It needs the variable where the data is stored in seq1, the value of pi, and the number of chromosomes

	k3=0

	##Pi within populations
	pw=(pi1+pi2)/2
	#print 'pw', pw

	for i in xrange(len(seq1)):
		for j in xrange(len(seq2)):
			k3=k3+hamming_distance(seq1[i],seq2[j])

	##Pi between populations
	pb=k3/(float(nseq1)*float(nseq2))
	#print 'pb', pb

	if (pb==0):
		#return 'NA'
		return '0'

	else:
		fst=float(1-(pw/pb))
		return fst

##this function is used to calculate pi (nucleotide diversity)
def hamming_distance(s1, s2):
	#Hamming distance between two strings of equal length is the number of positions at which the corresponding symbols are different

	assert len(s1) == len(s2)
	return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

##Function to calculate the number of segregating sites, doubletons and singletons
def base_S_ss(seq,nbsites):

	##spec_zero is the site frequency spectrum
	spec_zero=[]
	for g in range(len(seq)-1):
		spec_zero.append(0)

	var_ss=0 #Segregating sites

	alleles=zip(*seq)
	for g in range(nbsites):
		if 0<list(alleles[g]).count('1')<(list(alleles[g]).count('1')+list(alleles[g]).count('0')): ##this ignores sites that have all zeros, or all ones
			var_ss=var_ss+1
			spec_zero[list(alleles[g]).count('1')-1]=spec_zero[list(alleles[g]).count('1')-1]+1

	if var_ss>0:
		Ns=spec_zero[0]+spec_zero[-1] ##number of singletons
		Nd=spec_zero[1]+spec_zero[-2] ##number of doubletons
	else:
		Ns=0
		Nd=0

	return [var_ss,Ns,Nd,spec_zero]

##Function to calculate the number of different haplotypes
def base_h_ss(seq):

	#A set is an unordered collection with no duplicate elements
	haps=list(set(seq)) #makes a list of the different haplotypes found in seq
	#print haps
	cnt=[]
	hi=0 #hi is the mode of the haplotypes
	for g in range(len(haps)):
		cnt.append(seq.count(haps[g])) #count() returns the number of occurrences of substring sub (in this case, haplotypes) in seq
		#print cnt[g]
		if cnt[g]>hi: #you are keeping the highest occurrence of all the haplotypes, which the mode
			hi=cnt[g]

	#this function returns the number of different haplotypes and the mode
	return [len(haps),hi]


##Function to calculate Fst (if the data is stored as genotypes)
def FST(geno1,geno2,nbsites):
	r=2.0
	n1=float(len(geno1))#how many individuals
	n2=float(len(geno2))
	n_bar=(n1/r)+(n2/r)#average sample size
	nc=((r*n_bar)-(((n1**2)/(r*n_bar))+((n2**2)/(r*n_bar))))/(r-1.0)

	a_sum=0.0
	abc_sum=0.0

	nsit_us=0
	for g in range(nbsites):
		#print g
		aa1=0 #how many homozygotes 0 in 1
		ab1=0 #how many heterozygotes in 1
		bb1=0 #how many homozygotes 1 in 1
		aa2=0
		ab2=0
		bb2=0
		for n in range(len(geno1)):
			ball=geno1[n][0][g].count('1')+geno1[n][1][g].count('1')#to get the information from geno, count how many ones in one pair
			if ball==0:
				aa1=aa1+1
			elif ball==1:
				ab1=ab1+1
			elif ball==2:
				bb1=bb1+1
		for n in range(len(geno2)):
			ball=geno2[n][0][g].count('1')+geno2[n][1][g].count('1')
			if ball==0:
				aa2=aa2+1
			elif ball==1:
				ab2=ab2+1
			elif ball==2:
				bb2=bb2+1

		if 0<aa1+aa2<(len(geno1)+len(geno2)):
			#print 'if'

			p1=float((bb1*2.0)+ab1)/(n1*2.0)#get frequency of the derived allele in the two populations
			#print 'p1'
			#print p1
			p2=float((bb2*2.0)+ab2)/(n2*2.0)
			#print 'p2'
			#print p2
			p_bar=((n1*p1)/(r*n_bar))+((n2*p2)/(r*n_bar)) #average allele frequency for that site
			#print 'p_bar'
			#print p_bar
			s_sq=(n1*((p1-p_bar)**2.0))/((r-1.0)*n_bar)+(n2*((p2-p_bar)**2.0))/((r-1.0)*n_bar)
			#print 's_sq'
			#print s_sq
			h1=float(ab1)/n1 #frequency of the heterozygotes in population 1
			#print 'h1'
			#print h1
			h2=float(ab2)/n2
			##print 'h2'
			#print h2
			h_bar=((n1*h1)/(r*n_bar))+((n2*h2)/(r*n_bar))
			#print 'h_bar'
			#print h_bar

			a=(n_bar/nc)*((s_sq)-(1.0/(n_bar-1.0))*((p_bar*(1.0-p_bar))-(((r-1.0)/r)*s_sq)-(h_bar/4.0)))
			#print 'a'
			#print a
			b=(n_bar/(n_bar-1))*(((p_bar)*(1.0-p_bar))-(((r-1.0)/r)*s_sq)-((((2.0*n_bar)-1)/(4*n_bar))*h_bar))
			#print 'b'
			#print b
			c=(1.0/2.0)*h_bar
			#print 'c'
			#print c

			a_sum=a_sum+a
			abc_sum=abc_sum+(a+b+c)
			#print 'abc_sum'
			#print abc_sum

			nsit_us=nsit_us+1
		else:
			nsit_us=nsit_us

	if abc_sum==0.0:
		theta='NA'
	else:
		theta=a_sum/abc_sum

	return theta

##Function to calculate Tajima's D
def Tajimas(p,S,n):
	###It need pi, number of segregating sites, and number of chromosomes

	if (S==0):
		return 0

	else:

		a1=0.0
		for g in range(n-1):
			a1=a1+(1.0/(g+1.0))

		#print a1
		a2=0.0
		for g in range(n-1):
			a2=a2+(1.0/((g+1.0)**2))

		b1=(n+1.0)/(3.0*(n-1.0))

		b2=(2*((n**2.0)+n+3))/((9*n)*(n-1))
		c1=b1-(1.0/a1)
		#print 'c1', c1
		c2=b2-((n+2.0)/(a1*n))+(a2/(a1**2.0))
		e1=c1/a1
		#print 'e1', e1
		e2=c2/((a1**2.0)+a2)
		#print 'e2', e2
		TajD=(p-(S/a1))/(sqrt((e1*S)+((e2*S)*(S-1.0))))

		return TajD

d##Function to calculate the number of private and shared haplotypes in populations
def pri_sha_h(seqs1,seqs2):

	priA=0
	priB=0
	sha=0

	#this puts the two sequences together in the same array
	seqs=seqs1[:]
	seqs.extend(seqs2)
	haps=list(set(seqs)) #makes a list of the different haplotypes found in all populations

	for g in range(len(haps)):
		pop1_cnt=seqs1.count(haps[g]) #count() returns the number of occurrences of substring sub (in this case, haplotypes) in seqs1, which population 1
		pop2_cnt=seqs2.count(haps[g])

		#print pop1_cnt
		#print pop2_cnt

		if pop1_cnt>0 and pop2_cnt>0:
			sha=sha+1
		elif pop1_cnt>0 and pop2_cnt==0:
			priA=priA+1
		elif pop1_cnt==0 and pop2_cnt>0:
			priB=priB+1

	return [sha,priA,priB]

###end summary statistics###################################
############################################################

##Output file that will contain the summary statistics
outfile=open('regions_ss_allpops.txt','w')

##head of the file with the summary statistics
head='SegS_Af_CGI_m\tSing_Af_CGI_m\tDupl_Af_CGI_m\tTajD_Af_CGI_m\t'
head=head+'SegS_Eu_CGI_m\tSing_Eu_CGI_m\tDupl_Eu_CGI_m\tTajD_Eu_CGI_m\t'
head=head+'SegS_As_CGI_m\tSing_As_CGI_m\tDupl_As_CGI_m\tTajD_As_CGI_m\t'

head=head+'FST_AfEu_CGI_m\tFST_AfAs_CGI_m\tFST_EuAs_CGI_m\t'

head=head+'Nb_diff_hap_Af_CGI_m\tMode_hap_Af_CGI_m\t'
head=head+'Nb_diff_hap_Eu_CGI_m\tMode_hap_Eu_CGI_m\t'
head=head+'Nb_diff_hap_As_CGI_m\tMode_hap_As_CGI_m\t'

head=head+'Nb_shared_hap_AfEu_CGI_m\tPriv_hap_Af1_CGI_m\tPriv_hap_Eu1_CGI_m\t'
head=head+'Nb_shared_hap_AfAs_CGI_m\tPriv_hap_Af2_CGI_m\tPriv_hap_As1_CGI_m\t'
head=head+'Nb_shared_hap_EuAs_CGI_m\tPriv_hap_Eu2_CGI_m\tPriv_hap_As2_CGI_m\t'

head=head+'SegS_Af_CGI_sd\tSing_Af_CGI_sd\tDupl_Af_CGI_sd\tTajD_Af_CGI_sd\t'
head=head+'SegS_Eu_CGI_sd\tSing_Eu_CGI_sd\tDupl_Eu_CGI_sd\tTajD_Eu_CGI_sd\t'
head=head+'SegS_As_CGI_sd\tSing_As_CGI_sd\tDupl_As_CGI_sd\tTajD_As_CGI_sd\t'

head=head+'FST_AfEu_CGI_sd\tFST_AfAs_CGI_sd\tFST_EuAs_CGI_sd\t'

head=head+'Nb_diff_hap_Af_CGI_sd\tMode_hap_Af_CGI_sd\t'
head=head+'Nb_diff_hap_Eu_CGI_sd\tMode_hap_Eu_CGI_sd\t'
head=head+'Nb_diff_hap_As_CGI_sd\tMode_hap_As_CGI_sd\t'

head=head+'Nb_shared_hap_AfEu_CGI_sd\tPriv_hap_Af1_CGI_sd\tPriv_hap_Eu1_CGI_sd\t'
head=head+'Nb_shared_hap_AfAs_CGI_sd\tPriv_hap_Af2_CGI_sd\tPriv_hap_As1_CGI_sd\t'
head=head+'Nb_shared_hap_EuAs_CGI_sd\tPriv_hap_Eu2_CGI_sd\tPriv_hap_As2_CGI_sd\n'


outfile.write(head)

#############
ss_cnt=75 ##how many summary statistics
nbseq=1386 ##number of regions

###results will store the summary statistics
results=np.zeros((ss_cnt,nbseq),dtype='float32')

##sample sizes
##Africa
naf=18
##Europe
neu=18
##Asia
nas=8

#################

##Directory with the 10 kb regions
os.chdir("/home/u25/cdquinto/regions_NRE_trunc_mask_5percent/")
##Get the list of files inside that directory
array=os.listdir("/home/u25/cdquinto/regions_NRE_trunc_mask_5percent")
##reg_use is a counter for the number of regions
reg_use=0

##For each of the files in the directory
for i in range(len(array)):

	##read the file
	infile=array[i]
	file=open(infile)
	data=file.read()
	data=string.split(data,'\n')
	file.close()

	##remove the last end of line
	del(data[-1])

	nbss=len(data[0])

	##This transposes the data
	alleles=zip(*data)

	##Get the data for each of the populations
	seqAf=data[0:naf]
	#print seqAf
	seqEu=data[naf:naf+neu]
	#print seqEu
	seqAs=data[naf+neu:naf+neu+nas]
	#print seqAs

	##The number of segregating sites have to be greater than 0
	if nbss>0:

		##Calculate the summary statistics
		res=[]
		Af_res=[]
		Af_res.extend(base_S_ss(seqAf,nbss))
		pi_Af=Pi2(Af_res[3],len(seqAf))
		Af_res.append(Tajimas(pi_Af,Af_res[0],len(seqAf)))
		del(Af_res[3])
		res.extend(Af_res)

		Eu_res=[]
		Eu_res.extend(base_S_ss(seqEu,nbss))
		pi_Eu=Pi2(Eu_res[3],len(seqEu))
		Eu_res.append(Tajimas(pi_Eu,Eu_res[0],len(seqEu)))
		del(Eu_res[3])
		res.extend(Eu_res)

		As_res=[]
		As_res.extend(base_S_ss(seqAs,nbss))
		pi_As=Pi2(As_res[3],len(seqAs))
		As_res.append(Tajimas(pi_As,As_res[0],len(seqAs)))
		del(As_res[3])
		res.extend(As_res)

		##fst between populations
		res.append(FST2(seqAf,pi_Af,naf,seqEu,pi_Eu,neu))
		res.append(FST2(seqAf,pi_Af,naf,seqAs,pi_As,nas))
		res.append(FST2(seqEu,pi_Eu,neu,seqAs,pi_As,nas))

		##get haplotype stats with data WITH SINGLETONS
		res.extend(base_h_ss(seqAf))
		res.extend(base_h_ss(seqEu))
		res.extend(base_h_ss(seqAs))

		##shared and private haplotypes with data WITH SINGLETONS
		res.extend(pri_sha_h(seqAf,seqEu))
		res.extend(pri_sha_h(seqAf,seqAs))
		res.extend(pri_sha_h(seqEu,seqAs))


	else:
		print "error"

	##Put all the summary statistics into a matrix
	if 'NA' not in res:
		ss_add=0
		for n in range(len(res)):
			results[ss_add][reg_use]=res[n]
			ss_add=ss_add+1

		reg_use=reg_use+1

	#print reg_use

########################
########################
#Print the file with the summary statistics

out=''

for g in range(len(results)):
	#print g
	out=out+str(np.mean(results[g]))+'\t'
for g in range(len(results)):
	out=out+str(np.std(results[g]))+'\t'

out=out[:-1]+'\n'


outfile.write(out)
outfile.close()

np.savetxt('/home/u25/cdquinto/regions_matrix_new.txt', zip(*results), fmt='%f', delimiter='\t')
