#######Transformation into PLS (using ABCtoolbox)

Rscript --vanilla ~/bin/find_pls.r /xdisk/cdquinto/GOOD_FILES_SIMS/ input_1001069sims_ABCestimator_regions_Affy6.0.txt 21 212 2 20
transformer Routput_input_1001069sims_ABCestimator_regions_Affy6.0.txt input_1001069sims_ABCestimator_regions_Affy6.0.txt input_1001069sims_ABCestimator_regions_Affy6.0_transformer.txt boxcox
transformer Routput_input_1001069sims_ABCestimator_regions_Affy6.0.txt regions_5percent_ss_10kb_Affy6.0_snps.txt regions_5percent_ss_10kb_Affy6.0_snps_transformer.txt boxcox

#######Make files with different number of PLS

#######File with the simulations
for i in 25 26 27 28 29 30 31 32 33 34 35 40 50 70; do c=0; c=`expr $i - 20`; echo $c; cut -f-${i} /xdisk/cdquinto/GOOD_FILES_SIMS/input_536864sims_ABCestimator_regions_Affy6.0_transformer.txt > input_536864sims_ABCestimator_regions_Affy6.0_transformer_${c}pls.txt; done

#######File with the observed summary statistics

for i in 5 6 7 8 9 10 11 12 13 14 15 20 30 50; do  cut -f-${i} regions_5percent_ss_10kb_Affy6.0_snps_transformer.txt > regions_5percent_ss_10kb_Affy6.0_snps_transformer_${i}pls.txt; done

########Make directories for each combination of PLS and number of retained simulations (sims)
for pls in 5 6 7 8 9 10 11 12 13 14 15 20 30 50; do for sims in 100 500 1000 2000 5000;do mkdir res_${pls}pls_${sims}retsims; done; done

########Bootstrap the simulations and run ABCtoolbox
for pls in 5 6 7 8 9 10 11 12 13 14 15 20 30 50; do for retsims in 100 500 1000 2000 5000; do for i in {1..1000}; do echo "python test_bootstrap_seed_MXL.py ${pls} ${retsims} regions_5percent_ss_10kb_Affy6.0_snps_transformer_${pls}pls.txt input_536864sims_ABCestimator_regions_Affy6.0_transformer_${pls}pls.txt ${i}" >> commands_${pls}pls_${retsims}retsims.txt; done ; done ; done

########Make PBS files to run in supercomputer
i=0; for pls in 5 6 7 8 9 10 11 12 13 14 15 20 30 50; do for retsims in 100 500 1000 2000 5000; do i=$(($i+1)); perl ~/scripts/makePBSfile.pl boot_$i 1000 standard htc commands_${pls}pls_${retsims}retsims.txt 05; done; done

perl get_mode_bootstrap.pl

for pls in 5 6 7 8 9 10 11 12 13 14 15 20 30 50; do for retsims in 100 500 1000 2000 5000; do Rscript get_quantile_bootstrap.R res_${pls}pls_${retsims}retsims/mode_param_${pls}pls_${retsims}retsims.txt; done; done

for pls in 5 6 7 8 9 10 11 12 13 14 15 20 30 50; do for retsims in 100 500 1000 2000 5000; do python run_ABCestimator_beta.py regions_5percent_ss_10kb_Affy6.0_snps_transformer_${pls}pls.txt input_526070sims_ABCestimator_regions_Affy6.0_transformer_${pls}pls.txt ${pls} ${retsims} 526070 | ABCtoolbox_beta2 /dev/stdin; done; done

for pls in 5 6 7 8 9 10 11 12 13 14 15 20 30 50; do for retsims in 100 500 1000 2000 5000; do perl change_ABCtoolbox_beta_file_MXL.pl results_regions_5percent_ss_10kb_Affy6.0_snps_transformer_${pls}pls.txt_${retsims}_model0_MarginalPosteriorCharacteristics.txt; done; done

mkdir res_obs_data;
mv results_regions_5percent_ss_10kb_Affy*_transformer_* res_obs_data/

perl get_modes_obsdata.pl

mv modes_obsdata_* res_obs_data/

perl make_file_quantile_bootstrap.pl

for i in 5 6 7 8 9 10 11 12 13 14 15 20; do for j in 100 500 1000 2000 5000; do mkdir sims_${i}pls_${j}retsims; done; done

Rscript get_values_sims_bootstrap_cov.R

#####Made this script: run_sims_MEX_adm_model_Affy6.0_posterior_estimates.py

perl make_file_run_sims_MEX_adm_mode_posterior_estimates.pl

chmod 777 commands*

i=0; for pls in 5 6 7 8 9 10 11 12 13 14 15 20; do for retsims in 100 500 1000 2000 5000; do i=$(($i+1)); perl ~/scripts/makePBSfile.pl boot_$i 1000 standard cluster commands_${pls}pls_${retsims}retsims.txt 05; done; done

for i in 5 6 7 8 9 10 11 12 13 14 15 20; do for j in 100 500 1000 2000 5000; do cd sims_${i}pls_${j}retsims; for file in `ls *.summary`; do sed -n 2p ${file} >> all_results_sims_${i}pls_${j}retsims.txt; done; cd ..; done; done

for i in 5 6 7 8 9 10 11 12 13 14 15 20; do for j in 100 500 1000 2000 5000; do cd sims_${i}pls_${j}retsims; mv all_results_sims_${i}pls_${j}retsims.txt ..; cd ..; done; done

for file in `ls all_results_sims_*`; do cat /xdisk/cdquinto/GOOD_FILES_SIMS/regions_5percent_ss_10kb_Affy6.0_snps.txt ${file} > obs_ss_${file}; done

for i in 100 1000; do python run_ABCestimator_beta.py regions_5percent_ss_10kb_Affy6.0_snps_transformer_20pls.txt input_536864sims_ABCestimator_regions_Affy6.0_transformer_20pls_flat.txt 20 $i 536864 | ABCtoolbox_beta2 /dev/stdin; done
for i in 100 1000; do Rscript plot_posterior_ABtoolbox_new.R regions_5percent_ss_10kb_Affy6.0_snps_transformer_20pls.txt test results_regions_5percent_ss_10kb_Affy6.0_snps_transformer_20pls.txt_${i}_model0_MarginalPosteriorDensities_Obs0.txt results_regions_5percent_ss_10kb_Affy6.0_snps_transformer_20pls.txt_${i}_model0_BestSimsParamStats_Obs0.txt; mv out.pdf plot_posterior_densities_20pls_${i}retsims.pdf; done
