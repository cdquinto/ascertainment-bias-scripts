for file in `ls *.tped`
do
	cut -d" " -f5- ${file} > ${file}_out
	python -c "import sys; print('\n'.join(' '.join(c) for c in zip(*(l.split() for l in sys.stdin.readlines() if l.strip()))))" < ${file}_out > ${file}_out2
	perl -lape 's/\s+//sg' ${file}_out2 > ${file}_out3 
	mv ${file}_out3 ${file}
	rm *.tped_out*
done