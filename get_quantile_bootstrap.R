##Script to calculate the quantiles of the modes
##Author: Consuelo Quinto
##Jan 31, 2017

args <- commandArgs(TRUE);

argument<-args[1]
temp<-strsplit(argument, "/")
dir<-temp[[1]][1]

table <- read.delim(argument, header=TRUE)
header<-c("Asc_NAF","Asc_NEU","Asc_NCHB","daf","Log10_NAF","Log10_NEU","Log10_NCHB","NEU_AS","TEU_AS","TAF","Log10_NAT","Log10_NIBS","Log10_NMEX","TCEU_IBS","TCHB_NAT","TADM","PADM")

for (j in 1:ncol(table)){
a<-vector()
c<-header[j]
a<-c(a,c)
v<-table[,j]
q<-quantile(v,c(0.025,0.975))
a<-c(a,q[1])
a<-c(a,q[2])
out<-paste(dir,"/quantile_95.txt",sep="")
print(out)
write.table(matrix(a,nrow=1), file=out ,quote=FALSE,sep="\t", row.names=FALSE, col.names=FALSE, append=TRUE)
}
