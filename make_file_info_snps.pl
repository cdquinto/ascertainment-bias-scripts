#!/usr/bin/perl
use warnings;
##Script to make a new file with the information of each of the regions (based on whether there were SNPs in the regions)
##Author: Consuelo Quinto
##Feb 21, 2017

##It needs the name of the output file
$output_file=$ARGV[0];
open(OUT,">$output_file");

open(FILE,"regions_NRE_trunc_mask_no_missing_5percent.txt");
while(<FILE>){
	chomp;
	@split=split(/\t/,$_);
	$chr=$split[0];
	$region=$split[3];
	##HAVE TO CHANGE THE NAME OF THE DIRECTORY AND OF THE FILES
	##Example: directory_name/region_${split[3]}_chr${split[0]}.bim

	##If this file does not exist, then it means that that particular region did not have any SNPs that were also in the SNP array
	if (-e "directory_name/region_${split[3]}_chr${split[0]}.bim"){
		##get the number of lines, therefore number of SNPs from the bim file
		$lines=`wc -l directory_name/region_${split[3]}_chr${split[0]}.bim`;
		@count=split(/\s/,$lines);
		##print the chromosome, the start and end of the region, the ID of the region, the lenght of the region, and a flag
		##the flag equals one if there were SNPs
		##the last element that is printed is the number of SNPs in that region
		print OUT "$chr\t$split[1]\t$split[2]\t$region\t$split[4]\t1\t$count[0]\n";
	}
	else{
		##in this case, the flag is equal to 0 since there were no SNPs, and the count of SNPs is zero
		print OUT "$chr\t$split[1]\t$split[2]\t$region\t$split[4]\t0\t0\n";
	}
}
close(FILE);
close(OUT);
