#!/usr/bin/perl
use warnings;
##Script to get the SNP array sites for each of the regions in regions_NRE_trunc_mask_no_missing_5percent.txt
##Author: Consuelo Quinto
##Jan 27, 2017

##Make directory to store the results
system("mkdir directory_name");

##Open file that will contain how many SNPs are in each of the regions
open(OUT,">regions_NRE_trunc_mask_no_missing_5percent_snp_info.txt");

##Read the file with the regions
open(FILE,"regions_NRE_trunc_mask_no_missing_5percent.txt");
while(<FILE>){
	chomp;
	@split=split(/\t/,$_);

	##Write a bed-like list of the 10kb regions and the number of SNPS in each of them
	open(TXT,">directory_name/region_${split[3]}_chr${split[0]}_snps.txt");

	##Use plink to find the SNPs is each of the regions
    system("plink2 --bfile SNP_array_file --chr $split[0] --from-bp $split[1] --to-bp $split[2] --make-bed --out directory_name/region_${split[3]}_chr${split[0]}");

    ##If the bim file exists, then write in the OUT file that there are SNPs in the region (flag=1)
 	if (-e "directory_name/region_${split[3]}_chr${split[0]}.bim"){
 		print OUT "$split[0]\t$split[1]\t$split[2]\t$split[3]\t1\n";

 		##Remove the bed/fam/log files produced by plink
 		system("rm directory_name/region_${split[3]}_chr${split[0]}.bed");
    	system("rm directory_name/region_${split[3]}_chr${split[0]}.fam");
    	system("rm directory_name/region_${split[3]}_chr${split[0]}.log");

 		print TXT "chr${split[0]}\t$split[1]\t$split[2]\n$split[3]\n";
 	}

 	##If the bim does not exist, then there were no SNPs in the region (flag=0)
	else{
		print OUT "$split[0]\t$split[1]\t$split[2]\t$split[3]\t0\n";
		system("rm directory_name/region_${split[3]}_chr${split[0]}.log");
 	}
 	close(TXT);
}
close(FILE);
close(OUT);
