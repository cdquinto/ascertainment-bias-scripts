##Script to calculate the summary statistics of the microarray SNPs
##Author: Consuelo Quinto, Krishna Veeramah
##Jan 31, 2017

import subprocess
from subprocess import Popen,PIPE,call
import os
import string
from string import join
import math
import random
from random import randint
from random import uniform
from random import gauss
from random import gammavariate
from random import betavariate
from math import sqrt
import sys
from sys import argv
import datetime
from bisect import bisect_left
from bisect import bisect_right
import numpy as np
import re

######All the functions in this part calculate the summary statistics

##this function is used to calculate pi (nucleotide diversity)
def hamming_distance(s1, s2):
	#Hamming distance between two strings of equal length is the number of positions at which the corresponding symbols are different

	assert len(s1) == len(s2)
	return sum(ch1 != ch2 for ch1, ch2 in zip(s1, s2))

##Function to calculate the number of segregating sites, doubletons and singletons
def base_S_ss(seq,nbsites):

	##spec_zero is the site frequency spectrum
	spec_zero=[]
	for g in range(len(seq)-1):
		spec_zero.append(0)

	var_ss=0 #Segregating sites

	alleles=zip(*seq)
	for g in range(nbsites):
		if 0<list(alleles[g]).count('1')<(list(alleles[g]).count('1')+list(alleles[g]).count('0')): ##this ignores sites that have all zeros, or all ones
			var_ss=var_ss+1
			spec_zero[list(alleles[g]).count('1')-1]=spec_zero[list(alleles[g]).count('1')-1]+1

	if var_ss>0:
		Ns=spec_zero[0]+spec_zero[-1] ##number of singletons
		Nd=spec_zero[1]+spec_zero[-2] ##number of doubletons
	else:
		Ns=0
		Nd=0

	return [var_ss,Ns,Nd,spec_zero]

##Function to calculate the number of different haplotypes
def base_h_ss(seq):

	#A set is an unordered collection with no duplicate elements
	haps=list(set(seq)) #makes a list of the different haplotypes found in seq
	#print haps
	cnt=[]
	hi=0 #hi is the mode of the haplotypes
	for g in range(len(haps)):
		cnt.append(seq.count(haps[g])) #count() returns the number of occurrences of substring sub (in this case, haplotypes) in seq
		#print cnt[g]
		if cnt[g]>hi: #you are keeping the highest occurrence of all the haplotypes, which the mode
			hi=cnt[g]

	#this function returns the number of different haplotypes and the mode
	return [len(haps),hi]

##Function to calculate nucleotide diversity
def Pi2(spec,n): #standard pi, n = sample size (in chromosomes) and spec is the site frequency spectrum
	theta_pi=0.0

	for g in range(len(spec)):
		theta_pi=theta_pi+(2.0*float(spec[g])*(g+1.0)*(n-(g+1.0)))/(n*(n-1.0))

	return theta_pi

##Function to calculate Tajima's D
def Tajimas(p,S,n):
	###It need pi, number of segregating sites, and number of chromosomes

	if (S==0):
		return 0

	else:

		a1=0.0
		for g in range(n-1):
			a1=a1+(1.0/(g+1.0))

		#print a1
		a2=0.0
		for g in range(n-1):
			a2=a2+(1.0/((g+1.0)**2))

		b1=(n+1.0)/(3.0*(n-1.0))

		b2=(2*((n**2.0)+n+3))/((9*n)*(n-1))
		c1=b1-(1.0/a1)
		#print 'c1', c1
		c2=b2-((n+2.0)/(a1*n))+(a2/(a1**2.0))
		e1=c1/a1
		#print 'e1', e1
		e2=c2/((a1**2.0)+a2)
		#print 'e2', e2
		TajD=(p-(S/a1))/(sqrt((e1*S)+((e2*S)*(S-1.0))))

		return TajD

##Function to calculate Fst (if the data is stored as genotypes)
def FST(geno1,geno2,nbsites):
	r=2.0
	n1=float(len(geno1))#how many individuals
	n2=float(len(geno2))
	n_bar=(n1/r)+(n2/r)#average sample size
	nc=((r*n_bar)-(((n1**2)/(r*n_bar))+((n2**2)/(r*n_bar))))/(r-1.0)

	a_sum=0.0
	abc_sum=0.0

	nsit_us=0
	for g in range(nbsites):
		#print g
		aa1=0 #how many homozygotes 0 in 1
		ab1=0 #how many heterozygotes in 1
		bb1=0 #how many homozygotes 1 in 1
		aa2=0
		ab2=0
		bb2=0
		for n in range(len(geno1)):
			ball=geno1[n][0][g].count('1')+geno1[n][1][g].count('1')#to get the information from geno, count how many ones in one pair
			if ball==0:
				aa1=aa1+1
			elif ball==1:
				ab1=ab1+1
			elif ball==2:
				bb1=bb1+1
		for n in range(len(geno2)):
			ball=geno2[n][0][g].count('1')+geno2[n][1][g].count('1')
			if ball==0:
				aa2=aa2+1
			elif ball==1:
				ab2=ab2+1
			elif ball==2:
				bb2=bb2+1

		if 0<aa1+aa2<(len(geno1)+len(geno2)):
			#print 'if'

			p1=float((bb1*2.0)+ab1)/(n1*2.0)#get frequency of the derived allele in the two populations
			#print 'p1'
			#print p1
			p2=float((bb2*2.0)+ab2)/(n2*2.0)
			#print 'p2'
			#print p2
			p_bar=((n1*p1)/(r*n_bar))+((n2*p2)/(r*n_bar)) #average allele frequency for that site
			#print 'p_bar'
			#print p_bar
			s_sq=(n1*((p1-p_bar)**2.0))/((r-1.0)*n_bar)+(n2*((p2-p_bar)**2.0))/((r-1.0)*n_bar)
			#print 's_sq'
			#print s_sq
			h1=float(ab1)/n1 #frequency of the heterozygotes in population 1
			#print 'h1'
			#print h1
			h2=float(ab2)/n2
			##print 'h2'
			#print h2
			h_bar=((n1*h1)/(r*n_bar))+((n2*h2)/(r*n_bar))
			#print 'h_bar'
			#print h_bar

			a=(n_bar/nc)*((s_sq)-(1.0/(n_bar-1.0))*((p_bar*(1.0-p_bar))-(((r-1.0)/r)*s_sq)-(h_bar/4.0)))
			#print 'a'
			#print a
			b=(n_bar/(n_bar-1))*(((p_bar)*(1.0-p_bar))-(((r-1.0)/r)*s_sq)-((((2.0*n_bar)-1)/(4*n_bar))*h_bar))
			#print 'b'
			#print b
			c=(1.0/2.0)*h_bar
			#print 'c'
			#print c

			a_sum=a_sum+a
			abc_sum=abc_sum+(a+b+c)
			#print 'abc_sum'
			#print abc_sum

			nsit_us=nsit_us+1
		else:
			nsit_us=nsit_us

	if abc_sum==0.0:
		theta='NA'
	else:
		theta=a_sum/abc_sum

	return theta

##Function to calculate nucleotide diversity using the number of pairwise difference
def Pi(seq1,nseq1):
	##seq 1 are the stored data, and nseq1 is the number of chromosomes
	k1=0
	for i in xrange(0,nseq1):
		for j in xrange(i+1,nseq1):

			k1=k1+hamming_distance(seq1[i],seq1[j])

	p1=(2/(float(nseq1)*(float(nseq1)-1)))*k1

	return p1

##Function to calculate FST on pi within populations and between populations
def FST2(seq1,pi1,nseq1,seq2,pi2,nseq2):
	## It needs the variable where the data is stored in seq1, the value of pi, and the number of chromosomes

	k3=0

	##Pi within populations
	pw=(pi1+pi2)/2
	#print 'pw', pw

	for i in xrange(len(seq1)):
		for j in xrange(len(seq2)):
			k3=k3+hamming_distance(seq1[i],seq2[j])

	##Pi between populations
	pb=k3/(float(nseq1)*float(nseq2))
	#print 'pb', pb

	if (pb==0):
		#return 'NA'
		return '0'

	else:
		fst=float(1-(pw/pb))
		return fst

##Function to calculate the number of private and shared haplotypes in populations
def pri_sha_h(seqs1,seqs2):

	priA=0
	priB=0
	sha=0

	#this puts the two sequences together in the same array
	seqs=seqs1[:]
	seqs.extend(seqs2)
	haps=list(set(seqs)) #makes a list of the different haplotypes found in all populations

	for g in range(len(haps)):
		pop1_cnt=seqs1.count(haps[g]) #count() returns the number of occurrences of substring sub (in this case, haplotypes) in seqs1, which population 1
		pop2_cnt=seqs2.count(haps[g])

		#print pop1_cnt
		#print pop2_cnt

		if pop1_cnt>0 and pop2_cnt>0:
			sha=sha+1
		elif pop1_cnt>0 and pop2_cnt==0:
			priA=priA+1
		elif pop1_cnt==0 and pop2_cnt>0:
			priB=priB+1

	return [sha,priA,priB]

###end summary statistics###################################
############################################################

##cont is a counter of the number of summary statistics
cont=0
##reg_use is another counter for the number of regions
reg_use=0

ss_cnt=33 ##how many summary statistics
nbseq=1386 #number of regions

###results will store the summary statistics
results=np.zeros((ss_cnt,nbseq),dtype='float32')

####sample sizes
##Africa
naf=18
##Europe
neu=18
##Asia
nas=8

##This will read the file that has the information of the regions
##this file is the output of th script: make_file_info_snps.pl
list_regions = [line.strip() for line in open('NEW_regions_NRE_trunc_mask_no_missing_5percent_snp_info_OK_210116.txt')]

for line in list_regions:

	res=[]

	array=string.split(line,'\t')
	chr=array[0]
	region=array[3]
	length=array[4]

	##flag to tell if there are Affy SNPS on the region
	flag_snps=array[5]
	print 'SNPS in regions', flag_snps

	#####No Affy SNPs in the region, so just add zero's as ascertained summary statistics
	if (int(flag_snps)==0):

		print "No Affy SNPs in region", region

		for i in xrange(24):##3 from base_S_ss, 1 Pi, 1 Tajima's D, 3 FST, 6 haplotypes
			res.append(0)

		##shared and private haplotypes
		##YRI
		res.append(1)
		res.append(0)
		res.append(0)
		##CEU
		res.append(1)
		res.append(0)
		res.append(0)
		##CHB
		res.append(1)
		res.append(0)
		res.append(0)

		cont=cont+1

	elif (int(flag_snps)==1): ###there are Affy snps in the region

		##the infile is the region_*_chr*_allpops.txt
		infile='/data/Consuelo/CGI_asc_bias/NEW_Affy_snps_regions_NRE_trunc_mask_no_missing_5percent/regions_all_pops/region_'+str(region)+'_chr'+str(chr)+'_allpops.txt'

		#print infile

		file=open(infile)
		data=file.read()
		data=string.split(data,'\n')
		file.close()

		del(data[-1])
		print len(data)

		nbss=len(data[0])

		##44 is the number of chromosomes in all three considered populations
		if len(data)!=44:
			print "something is wrong"
			sys.exit()

		else:

			alleles=zip(*data)

			seqAf=data[0:naf]
			seqEu=data[naf:naf+neu]
			seqAs=data[naf+neu:naf+neu+nas]

			####calculate the summary statistics

			##Africa
			Af_res=[]
			Af_res.extend(base_S_ss(seqAf,nbss))
			pi_Af=Pi2(Af_res[3],len(seqAf))
			Af_res.append(pi_Af)
			Af_res.append(Tajimas(pi_Af,Af_res[0],len(seqAf)))
			del(Af_res[3])
			res.extend(Af_res)

			#Europa
			Eu_res=[]
			Eu_res.extend(base_S_ss(seqEu,nbss))
			pi_Eu=Pi2(Eu_res[3],len(seqEu))
			Eu_res.append(pi_Eu)
			Eu_res.append(Tajimas(pi_Eu,Eu_res[0],len(seqEu)))
			del(Eu_res[3])
			res.extend(Eu_res)

			#Asia
			As_res=[]
			As_res.extend(base_S_ss(seqAs,nbss))
			pi_As=Pi2(As_res[3],len(seqAs))
			As_res.append(pi_As)
			As_res.append(Tajimas(pi_As,As_res[0],len(seqAs)))
			del(As_res[3])
			res.extend(As_res)

			##Fst between populations
			res.append(FST2(seqAf,pi_Af,naf,seqEu,pi_Eu,neu))
			res.append(FST2(seqAf,pi_Af,naf,seqAs,pi_As,nas))
			res.append(FST2(seqEu,pi_Eu,neu,seqAs,pi_As,nas))

			##Haplotype statistics
			res.extend(base_h_ss(seqAf))
			res.extend(base_h_ss(seqEu))
			res.extend(base_h_ss(seqAs))

			##Shared and private haplotypes
			res.extend(pri_sha_h(seqAf,seqEu))
			res.extend(pri_sha_h(seqAf,seqAs))
			res.extend(pri_sha_h(seqEu,seqAs))

			cont=cont+1

	##Put all the summary statistics into a matrix
	if 'NA' not in res:
		ss_add=0
		for n in range(len(res)):
			results[ss_add][reg_use]=res[n]
			ss_add=ss_add+1

		reg_use=reg_use+1


##############
###Print outfiles with the summary statistics

outfile=open('regions_ss_allpops.txt','w')

##head of the file with the summary statistics
head='SegS_Af_ASC_m\tSing_Af_ASC_m\tDupl_Af_ASC_m\tPi_Af_ASC_m\tTajD_Af_ASC_m\t'
head=head+'SegS_Eu_ASC_m\tSing_Eu_ASC_m\tDupl_Eu_ASC_m\tPi_Eu_ASC_m\tTajD_Eu_ASC_m\t'
head=head+'SegS_As_ASC_m\tSing_As_ASC_m\tDupl_As_ASC_m\tPi_As_ASC_m\tTajD_As_ASC_m\t'

head=head+'FST_AfEu_ASC_m\tFST_AfAs_ASC_m\tFST_EuAs_ASC_m\t'

head=head+'Nb_diff_hap_Af_ASC_m\tMode_hap_Af_ASC_m\t'
head=head+'Nb_diff_hap_Eu_ASC_m\tMode_hap_Eu_ASC_m\t'
head=head+'Nb_diff_hap_As_ASC_m\tMode_hap_As_ASC_m\t'

head=head+'Nb_shared_hap_AfEu_ASC_m\tPriv_hap_Af1_ASC_m\tPriv_hap_Eu1_ASC_m\t'
head=head+'Nb_shared_hap_AfAs_ASC_m\tPriv_hap_Af2_ASC_m\tPriv_hap_As1_ASC_m\t'
head=head+'Nb_shared_hap_EuAs_ASC_m\tPriv_hap_Eu2_ASC_m\tPriv_hap_As2_ASC_m\t'

head=head+'SegS_Af_ASC_sd\tSing_Af_ASC_sd\tDupl_Af_ASC_sd\tPi_Af_ASC_sd\tTajD_Af_ASC_sd\t'
head=head+'SegS_Eu_ASC_sd\tSing_Eu_ASC_sd\tDupl_Eu_ASC_sd\tPi_Eu_ASC_sd\tTajD_Eu_ASC_sd\t'
head=head+'SegS_As_ASC_sd\tSing_As_ASC_sd\tDupl_As_ASC_sd\tPi_As_ASC_sd\tTajD_As_ASC_sd\t'

head=head+'FST_AfEu_ASC_sd\tFST_AfAs_ASC_sd\tFST_EuAs_ASC_sd\t'

head=head+'Nb_diff_hap_Af_ASC_sd\tMode_hap_Af_ASC_sd\t'
head=head+'Nb_diff_hap_Eu_ASC_sd\tMode_hap_Eu_ASC_sd\t'
head=head+'Nb_diff_hap_As_ASC_sd\tMode_hap_As_ASC_sd\t'

head=head+'Nb_shared_hap_AfEu_ASC_sd\tPriv_hap_Af1_ASC_sd\tPriv_hap_Eu1_ASC_sd\t'
head=head+'Nb_shared_hap_AfAs_ASC_sd\tPriv_hap_Af2_ASC_sd\tPriv_hap_As1_ASC_sd\t'
head=head+'Nb_shared_hap_EuAs_ASC_sd\tPriv_hap_Eu2_ASC_sd\tPriv_hap_As2_ASC_sd\n'

outfile.write(head)

#################
##Write the summary statistics

out=''

for g in range(len(results)):
	#print g
	out=out+str(np.mean(results[g]))+'\t'
for g in range(len(results)):
	out=out+str(np.std(results[g]))+'\t'

out=out[:-1]+'\n'

outfile.write(out)
outfile.close()

np.savetxt('regions_matrix.txt', zip(*results), fmt='%f', delimiter='\t')
