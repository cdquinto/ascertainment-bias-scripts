##Script to make a projection of the observed summary statistics in the simulated summary statistics
##Author: Consuelo Quinto
##Jan 31, 2017

args<-commandArgs(TRUE)

library("pracma");

#########
euclid <- function(x, y) {
len <- length(y[,1])
s <- 0
for (i in 1:len) {
s <- s + sqrt(sum((x - y[i,])^2))
}
s
}
#########

filename<-args[1]

results<-c()

results<-c(results,filename)

Tbl <-read.table(filename, header=TRUE)

##scale the data
scaled<-scale(Tbl,center=TRUE,scale=TRUE)

nb_col<-ncol(Tbl)

##Get the observed data
obs<-Tbl[1,]
obs_scaled<-scaled[1,]

##Get the simulations
rest<-Tbl[2:1000,1:nb_col]
rest_scaled<-scaled[2:1000,1:nb_col]

##Calculate the euclidean and mahalanobis distance

euclid1<-euclid(obs_scaled,rest_scaled)

mean<-colMeans(rest)

cov_mat<-cov(rest)

pseudo_inv<-pinv(cov_mat)

mahal<-mahalanobis(obs,mean,pseudo_inv, inverted=TRUE)

results<-c(results,euclid1)
results<-c(results,mahal)

write.table(matrix(results,nrow=1), file="euclid_mahal_dist_ss.txt" ,quote=FALSE,sep="\t", row.names=FALSE, col.names=FALSE, append=TRUE)
