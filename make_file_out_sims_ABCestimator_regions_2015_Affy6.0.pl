#!/usr/bin/perl
use strict;
use warnings;
#use List::MoreUtils qw(indexes);
use MoreUtils qw(indexes);

my $dir=$ARGV[0];

my $results_sims_dir=$ARGV[0]."/results_sims";
print "$results_sims_dir\n";

opendir(DIR,"$results_sims_dir") || die "Can't open that directory\n";

my @files = grep {/.summary$/} readdir(DIR); 
my $number=scalar(@files);
print "$number\n";
closedir(DIR);

open(OUT,">input_${number}sims_ABCestimator_regions_Affy6.0.txt");

print OUT "Sim\tAsc_NAF\tAsc_NEU\tAsc_NCHB\tdaf\tLog10_NAF\tLog10_NANC\tLog10_NCEU\tLog10_NCHB\tNEU_AS\tTEU_AS\tTAF\tTgrowth\tLog10_NAT\tLog10_NIBS\tLog10_NMEX\tTCEU_IBS\tTCHB_NAT\tTADM\tPADM\tSegS_Af_CGI_m\tSing_Af_CGI_m\tDupl_Af_CGI_m\tTajD_Af_CGI_m\tSegS_Eu_CGI_m\tSing_Eu_CGI_m\tDupl_Eu_CGI_m\tTajD_Eu_CGI_m\tSegS_As_CGI_m\tSing_As_CGI_m\tDupl_As_CGI_m\tTajD_As_CGI_m\tFST_AfEu_CGI_m\tFST_AfAs_CGI_m\tFST_EuAs_CGI_m\tNb_diff_hap_Af_CGI_m\tMode_hap_Af_CGI_m\tNb_diff_hap_Eu_CGI_m\tMode_hap_Eu_CGI_m\tNb_diff_hap_As_CGI_m\tMode_hap_As_CGI_m\tNb_shared_hap_AfEu_CGI_m\tPriv_hap_Af1_CGI_m\tPriv_hap_Eu1_CGI_m\tNb_shared_hap_AfAs_CGI_m\tPriv_hap_Af2_CGI_m\tPriv_hap_As1_CGI_m\tNb_shared_hap_EuAs_CGI_m\tPriv_hap_Eu2_CGI_m\tPriv_hap_As2_CGI_m\tSegS_Af_ASC_m\tSing_Af_ASC_m\tDupl_Af_ASC_m\tPi_Af_ASC_m\tTajD_Af_ASC_m\tSegS_Eu_ASC_m\tSing_Eu_ASC_m\tDupl_Eu_ASC_m\tPi_Eu_ASC_m\tTajD_Eu_ASC_m\tSegS_As_ASC_m\tSing_As_ASC_m\tDupl_As_ASC_m\tPi_As_ASC_m\tTajD_As_ASC_m\tSegS_Nat_ASC_m\tSing_Nat_ASC_m\tDupl_Nat_ASC_m\tPi_Nat_ASC_m\tTajD_Nat_ASC_m\tSegS_Ibs_ASC_m\tSing_Ibs_ASC_m\tDupl_Ibs_ASC_m\tPi_Ibs_ASC_m\tTajD_Ibs_ASC_m\tSegS_Mex_ASC_m\tSing_Mex_ASC_m\tDupl_Mex_ASC_m\tPi_Mex_ASC_m\tTajD_Mex_ASC_m\tFST_AfEu_ASC_m\tFST_AfAs_ASC_m\tFST_EuAs_ASC_m\tFST_NatIbs_ASC_m\tFST_NatMex_ASC_m\tFST_IbsMex_ASC_m\tNb_diff_hap_Af_ASC_m\tMode_hap_Af_ASC_m\tNb_diff_hap_Eu_ASC_m\tMode_hap_Eu_ASC_m\tNb_diff_hap_As_ASC_m\tMode_hap_As_ASC_m\tNb_diff_hap_Nat_ASC_m\tMode_hap_Nat_ASC_m\tNb_diff_hap_Ibs_ASC_m\tMode_hap_Ibs_ASC_m\tNb_diff_hap_Mex_ASC_m\tMode_hap_Mex_ASC_m\tNb_shared_hap_AfEu_ASC_m\tPriv_hap_Af1_ASC_m\tPriv_hap_Eu1_ASC_m\tNb_shared_hap_AfAs_ASC_m\tPriv_hap_Af2_ASC_m\tPriv_hap_As1_ASC_m\tNb_shared_hap_EuAs_ASC_m\tPriv_hap_Eu2_ASC_m\tPriv_hap_As2_ASC_m\tNb_shared_hap_NatIbs_ASC_m\tPriv_hap_Nat1_ASC_m\tPriv_hap_Ibs1_ASC_m\tNb_shared_hap_NatMex_ASC_m\tPriv_hap_Nat2_ASC_m\tPriv_hap_Mex1_ASC_m\tNb_shared_hap_IbsMex_ASC_m\tPriv_hap_Ibs2_ASC_m\tPriv_hap_Mex2_ASC_m\tSegS_Af_CGI_sd\tSing_Af_CGI_sd\tDupl_Af_CGI_sd\tTajD_Af_CGI_sd\tSegS_Eu_CGI_sd\tSing_Eu_CGI_sd\tDupl_Eu_CGI_sd\tTajD_Eu_CGI_sd\tSegS_As_CGI_sd\tSing_As_CGI_sd\tDupl_As_CGI_sd\tTajD_As_CGI_sd\tFST_AfEu_CGI_sd\tFST_AfAs_CGI_sd\tFST_EuAs_CGI_sd\tNb_diff_hap_Af_CGI_sd\tMode_hap_Af_CGI_sd\tNb_diff_hap_Eu_CGI_sd\tMode_hap_Eu_CGI_sd\tNb_diff_hap_As_CGI_sd\tMode_hap_As_CGI_sd\tNb_shared_hap_AfEu_CGI_sd\tPriv_hap_Af1_CGI_sd\tPriv_hap_Eu1_CGI_sd\tNb_shared_hap_AfAs_CGI_sd\tPriv_hap_Af2_CGI_sd\tPriv_hap_As1_CGI_sd\tNb_shared_hap_EuAs_CGI_sd\tPriv_hap_Eu2_CGI_sd\tPriv_hap_As2_CGI_sd\tSegS_Af_ASC_sd\tSing_Af_ASC_sd\tDupl_Af_ASC_sd\tPi_Af_ASC_sd\tTajD_Af_ASC_sd\tSegS_Eu_ASC_sd\tSing_Eu_ASC_sd\tDupl_Eu_ASC_sd\tPi_Eu_ASC_sd\tTajD_Eu_ASC_sd\tSegS_As_ASC_sd\tSing_As_ASC_sd\tDupl_As_ASC_sd\tPi_As_ASC_sd\tTajD_As_ASC_sd\tSegS_Nat_ASC_sd\tSing_Nat_ASC_sd\tDupl_Nat_ASC_sd\tPi_Nat_ASC_sd\tTajD_Nat_ASC_sd\tSegS_Ibs_ASC_sd\tSing_Ibs_ASC_sd\tDupl_Ibs_ASC_sd\tPi_Ibs_ASC_sd\tTajD_Ibs_ASC_sd\tSegS_Mex_ASC_sd\tSing_Mex_ASC_sd\tDupl_Mex_ASC_sd\tPi_Mex_ASC_sd\tTajD_Mex_ASC_sd\tFST_AfEu_ASC_sd\tFST_AfAs_ASC_sd\tFST_EuAs_ASC_sd\tFST_NatIbs_ASC_sd\tFST_NatMex_ASC_sd\tFST_IbsMex_ASC_sd\tNb_diff_hap_Af_ASC_sd\tMode_hap_Af_ASC_sd\tNb_diff_hap_Eu_ASC_sd\tMode_hap_Eu_ASC_sd\tNb_diff_hap_As_ASC_sd\tMode_hap_As_ASC_sd\tNb_diff_hap_Nat_ASC_sd\tMode_hap_Nat_ASC_sd\tNb_diff_hap_Ibs_ASC_sd\tMode_hap_Ibs_ASC_sd\tNb_diff_hap_Mex_ASC_sd\tMode_hap_Mex_ASC_sd\tNb_shared_hap_AfEu_ASC_sd\tPriv_hap_Af1_ASC_sd\tPriv_hap_Eu1_ASC_sd\tNb_shared_hap_AfAs_ASC_sd\tPriv_hap_Af2_ASC_sd\tPriv_hap_As1_ASC_sd\tNb_shared_hap_EuAs_ASC_sd\tPriv_hap_Eu2_ASC_sd\tPriv_hap_As2_ASC_sd\tNb_shared_hap_NatIbs_ASC_sd\tPriv_hap_Nat1_ASC_sd\tPriv_hap_Ibs1_ASC_sd\tNb_shared_hap_NatMex_ASC_sd\tPriv_hap_Nat2_ASC_sd\tPriv_hap_Mex1_ASC_sd\tNb_shared_hap_IbsMex_ASC_sd\tPriv_hap_Ibs2_ASC_sd\tPriv_hap_Mex2_ASC_sd\n";

my $sim_values_dir=$ARGV[0]."/sim_values";

foreach my $file (@files){
	#print "$file\n";
	my @split_name=split(/_/,$file);
	my @split_name2=split(/\./,$split_name[2]);
	my $i=$split_name2[0];
	#print "$i\n";
	
	my @indexes=();
	my @locs=();
	my @array=();
	
	open(SIMS,"${sim_values_dir}/sim_${i}_values.txt") || die "Can't open /sim_values_regions/sim_${i}_values.txt\n";
	readline(SIMS); #skip header 
	while(<SIMS>){
		chomp;
		push(@array,$_);
	}
	close(SIMS);
	
	my $ss_file="${results_sims_dir}/ms_output_${i}.summary";
	open(SS,"$ss_file") || die "Can't open $ss_file\n";	
	my $first_line=readline(SS);
	close(SS);
	
	if (! $first_line){
		print "Error in simulation $i\n";
		next; 
	}		
	
	else{
		print OUT "$i\t";
		
		foreach my $element (@array){
			print OUT "$element\t";
		}
		
		my $second_line=`sed -n '2p' ${ss_file}`;
		
		print OUT "$second_line";
		
	}
	
}
print "Done with $number simulations\n";
