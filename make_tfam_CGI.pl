#!/usr/bin/perl
use warnings;

foreach $pop ("YRI","CEU"){
	open (OUT,">testvariants_${pop}_regions_NRE_trunc_mask.tsv.tfam"); 
	for ($i=1;$i<19;$i++){
		print OUT "1\t$i\t0\t0\0\0\n";
	}
	close(OUT);
}

open (OUT,">testvariants_CHB_regions_NRE_trunc_mask.tsv.tfam");
for($i=1;$i<9;$i++){
	print OUT "1\t$i\t0\t0\0\0\n";
}
close(OUT);
	
		