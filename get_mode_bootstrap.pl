##Script to get the mode of the posterior estimates from the 1,000 iterations of bootstrap
##Author: Consuelo Quinto
##Jan 31, 2017

#!/usr/bin/perl
use warnings;

##These variables have to change in order to adjust the number of PLS and retained simulations
@pls_array=(8,9,10,11,12,13,14,15,20,30);
@retsims=(500,1000,4000);

foreach $pls (@pls_array){
	foreach $sims (@retsims){

		##Outfile
		$out="mode_param_".$pls."pls_".$sims."retsims.txt";
		open(OUT,">$out");

		$dir="res_".$pls."pls_".$sims."retsims";
		opendir(DIR,"$dir") || die "Can't open\n";

		##Get all the Posterior Estimates files
		@files=grep {/^results(\w+)PosteriorEstimates_Obs0.txt/} readdir(DIR);

		foreach $file (@files){
			open(FILE,"$dir/$file");
			readline(FILE); #skip the first line
			while(<FILE>){
				chomp;
				if ($.==2){ ## just need to read the first 2 lines (the second line contains the mode of the estimates)
					@split=split(/\t/,$_);
					shift(@split);
					for($i=0;$i<scalar(@split);$i++){
						if($i==(scalar(@split)-1)){
							print OUT "$split[$i]\n";
						}
						else{
							print OUT "$split[$i]\t";
						}
					}
				}
			}
			close(FILE);
		}
		close(OUT);
	}
}
