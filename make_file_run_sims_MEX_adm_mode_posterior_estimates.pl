#!/usr/bin/perl
use Cwd;
use warnings;

##Script to make a file with commands to run simulations
##Author: Consuelo Quinto
##Jan 31, 2017

##get the current directory
$current_dir = getcwd;

@pls_array=(5,6,7,8,9,10,11,12,13,14,15,20);
@retsims=(100,500,1000,2000,5000);

foreach $pls (@pls_array){
	foreach $sims (@retsims){

		open(FILE,"modes_sims_bootstrap_cov_${pls}pls_${sims}retsims.txt");

		open(OUT,">commands_${pls}pls_${sims}retsims.txt");
		$i=1;
		while(<FILE>){
			chomp;
			@split=split(/\t/,$_);
			print OUT "python run_sims_MEX_adm_model_Affy6.0_posterior_estimates.py $i $pls $sims ";
			for($j=0;$j<scalar(@split);$j++){
				print OUT "$split[$j] ";
			}
			print OUT "$current_dir\n";
			$i++;
		}
		close(FILE);
		close(OUT);
	}
}
