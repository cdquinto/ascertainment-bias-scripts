#!/usr/bin/perl
use warnings;

foreach $pop ("YRI","CEU"){
	print "$pop\n";
	opendir(DIR,"testvariants_pops") || die "Can't open that directory\n";
	@files = grep {/$pop\_region\_chr\d+_\d+\_snps.txt$/} readdir(DIR); 
	print scalar(@files),"\n";
	foreach $file (@files){
		open (OUT,">testvariants_pops/${file}.tfam"); 
		for($i=1;$i<10;$i++){
			print OUT "1\t$i\t0\t0\t0\t-9\n";
		}
		close(OUT);
	}
}

foreach $pop ("CHB"){
	opendir(DIR,"testvariants_pops") || die "Can't open that directory\n";
	@files = grep  {/$pop\_region\_chr\d+_\d+\_snps.txt$/}  readdir(DIR); 
	foreach $file (@files){
		open (OUT,">testvariants_pops/${file}.tfam");
		for($i=1;$i<5;$i++){
			print OUT "1\t$i\t0\t0\t0\t-9\n";
		}
		close(OUT);
	}
}

	
		