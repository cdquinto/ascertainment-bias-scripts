#!/usr/bin/perl
use warnings;
use Loops qw(MapCar);
##Script to change the format of the file with the posterior estimates
##Author: Consuelo Quinto
##Jan 31, 2017

$input_file=$ARGV[0];  #results_regions_5percent_ss_10kb_Affy_snps_transformer_30pls.txt_16000_model0_MarginalPosteriorCharacteristics.txt
# this file has 2 lines

@split_name=split(/Marginal/,$input_file);

@header=("mode","mean","median","quantile_50_lower_bound","quantile_50_upper_bound","quantile_90_lower_bound","quantile_90_upper_bound","quantile_95_lower_bound","quantile_95_upper_bound","quantile_99_lower_bound","quantile_99_upper_bound","HPD_50_lower_bound","HPD_50_upper_bound","HPD_90_lower_bound","HPD_90_upper_bound","HPD_95_lower_bound","HPD_95_upper_bound","HPD_99_lower_bound","HPD_99_upper_bound");

$nb_line=0;
open(FILE,$input_file) || die "Can't open $input_file\n";
readline(FILE); #skip the first line with the header
while(<FILE>){
	chomp;

	$out=$split_name[0]."PosteriorEstimates_Obs".$nb_line.".txt";
	open(OUT,">$out");

	@array=();
	$j=0;
	$k=0;

	@split=split(/\t/,$_);

	for($i=1;$i<scalar(@split);$i++){
		if ($i%19==0){
			$array[$k][$j]=$split[$i];
			$j=0;
			$k++;
		}
		else{
			$array[$k][$j]=$split[$i];
			$j++;
		}
	}

	@transposed = MapCar {[@_]} @array;
	print OUT "what\tAsc_NAF\tAsc_NEU\tAsc_NCHB\tdaf\tLog10_NAF\tLog10_NEU\tLog10_NCHB\tNEU_AS\tTEU_AS\tTAF\tLog10_NAT\tLog10_NIBS\tLog10_NMEX\tTCEU_IBS\tTCHB_NAT\tTADM\tPADM\n"; ##header of the file with the posterior estimates
	for($i=0;$i<19;$i++){
		print OUT "$header[$i]\t";
		for($j=0;$j<17;$j++){
			if ($j==16){
				print OUT "$transposed[$i][$j]\n";
			}
			else{
				print OUT "$transposed[$i][$j]\t";
			}
		}
	}
	close(OUT);
	$nb_line++;
}
close(FILE);
