##Script to get the mode of the posterior estimates from the 1,000 iterations of bootstrap
##Author: Consuelo Quinto
##Jan 31, 2017

#!/usr/bin/perl

use warnings;

@pls_array=(5,6,7,8,9,10,11,12,13,14,15,20,30,50);
@retsims=(100,500,1000,2000);

foreach $pls (@pls_array){
	foreach $sims (@retsims){

		##Output file
		$out="modes_obsdata_".$pls."pls_".$sims."retsims.txt";
		open(OUT,">$out");

		##Change the name of the file that has the posterior estimates
		$file="res_obs_data/results_regions_5percent_ss_10kb_AXIOM_LAT_snps_transformer_".$pls."pls.txt_".$sims."_model0_PosteriorEstimates_Obs0.txt";
		open(FILE,"$file") || die "Can't open $file\n";
		$first_line=readline(FILE);
		chomp($first_line);
		@header=split(/\t/,$first_line);
		shift(@header);

		while(<FILE>){
			chomp;
			if($_=~/^mode/){
				@split=split(/\t/,$_);
				shift(@split);
			}
		}
		close(FILE);

		for($i=0;$i<scalar(@split);$i++){
			print OUT "$header[$i]\t$split[$i]\n";
		}
		close(OUT);
	}
}
