##Script to bootstrap the summary statistics and use ABCtoolbox
##Author: Consuelo Quinto
##Jluy 6, 2016

import random
import math
import datetime
from subprocess import Popen,PIPE
import sys
from sys import argv
import os
from os import getpid
#from numpy import random
import random
from random import randint
from random import uniform
from random import gauss
from random import gammavariate
from random import betavariate
import string
import re
import glob
import datetime

####input arguments

pls=str(argv[1])

retsims=str(argv[2])

obs_file=str(argv[3]) ##file with the observed ss

sim_file=str(argv[4]) ##file with the simulations to run ABCestimator

rep=int(argv[5]) ##this is the id of the iteration of the bootstrap

directory='res_'+str(pls)+'pls_'+str(retsims)+'retsims'

##Reading simulations file
#print 'Read sim_file'
#print datetime.datetime.now()
sims_file=open(sim_file)
sims=sims_file.read()
sims=string.split(sims,'\n')
header_sims=sims[0]
del sims[0]
del sims[-1]
#print 'End sim_file'
#print datetime.datetime.now()

#print 'len (sims)', len(sims)

#####Bootstrap
##print the file with the bootstrapped simulations
filenameout='test_file_'+str(pls)+'PLS_'+str(retsims)+'retsims_'+str(rep)+'.txt'
file=open(filenameout,'w')
file.write(str(header_sims)+'\n')

####make the boostrap, select one line at random from the big file
##set the seed of the random generator
seed=int(rep)
#print 'seed', seed

rnd = random.Random(int(seed))
array_values=[rnd.randint(0, len(sims)-1) for i in xrange(len(sims))]

for j in xrange(len(array_values)):
	#print j
	file.write(str(sims[array_values[j]])+'\n')

file.close()

#print 'End bootstrap'
#print datetime.datetime.now()

dirac=0.01 #value of the dirac parameter
PDP=100 #posterior density points

####Print file for ABC
outABC=''
outABC=outABC+'task\testimate\n'
outABC=outABC+'estimationType\tstandard\n'
outABC=outABC+'simName\t'+str(filenameout)+'\n'
outABC=outABC+'obsName\t'+str(obs_file)+'\n'
outABC=outABC+'params\t2-6,8-12,14-20\n'
outABC=outABC+'numRetained\t'+str(retsims)+'\n'
outABC=outABC+'maxReadSims\t'+str(len(sims))+'\n'
outABC=outABC+'diracPeakWidth\t'+str(dirac)+'\nposteriorDensityPoints\t'+str(PDP)+'\nstandardizeStats\t0\nwriteRetained\t0\nobsPValue\t0\n'
outABC=outABC+'outputPrefix\t'+str(directory)+'/results_'+str(pls)+'PLS_'+str(retsims)+'retsims_'+str(rep)+'_\n'
outABC=outABC+'logFile\t'+str(directory)+'/ABC_results_'+str(pls)+'PLS_'+str(retsims)+'retsims_'+str(rep)+'.log'

inputfile='ABCtoolbox_test_file_'+str(pls)+'PLS_'+str(retsims)+'retsims_'+str(rep)+'.input'
file2=open(inputfile,'w')
file2.write(str(outABC))
file2.close()

###Run ABCtoolbox (has to be the last available version)
## https://bitbucket.org/phaentu/abctoolbox-public/
Popen.wait(Popen('ABCtoolbox '+str(inputfile),shell=True))

###Change format of the file with the posterior estimates
Popen.wait(Popen('perl change_ABCtoolbox_beta_file_MXL.pl '+str(directory)+'/results_'+str(pls)+'PLS_'+str(retsims)+'retsims_'+str(rep)+'_model0_MarginalPosteriorCharacteristics.txt', shell=True))

###Remove files
Popen.wait(Popen('rm '+str(filenameout),shell=True))
Popen.wait(Popen('rm '+str(inputfile),shell=True))
Popen.wait(Popen('rm '+str(directory)+'/results_'+str(pls)+'PLS_'+str(retsims)+'retsims_'+str(rep)+'_*Marginal*',shell=True))
Popen.wait(Popen('rm '+str(directory)+'/results_'+str(pls)+'PLS_'+str(retsims)+'retsims_'+str(rep)+'_modelFit.txt',shell=True))
Popen.wait(Popen('rm '+str(directory)+'/ABC_results_'+str(pls)+'PLS_'+str(retsims)+'retsims_'+str(rep)+'.log',shell=True))
