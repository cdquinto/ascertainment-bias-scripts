#!/usr/bin/perl
use warnings;
##Script to concatenate the files of the regions by populations
##Author: Consuelo Quinto
##Feb 21, 2017

##This script needs the file that was written by make_file_info_snps.pl
open(FILE,"output_file_name.txt") || die "Can't open file\n";
while(<FILE>){
	chomp;
	@split=split(/\t/,$_);
	##get the chromosome number
	$chr=$split[0];
	##get the flag of SNPs
	$flag=$split[5];
	print "$flag\n";
	##get ID of the region
	$region=$split[3];

	##if the flag is 1, then SNPs in regions

	##the three files from the three populations are concatenated
	##the output file goes into the directory regions_all_pops
	if ($flag eq "1"){
		system("cat chr${chr}/YRI_region_${region}_chr${chr}_snps.txt.tped chr${chr}/CEU_region_${region}_chr${chr}_snps.txt.tped chr${chr}/CHB_region_${region}_chr${chr}_snps.txt.tped > regions_all_pops/region_${region}_chr${chr}_allpops.txt");
	}
}
close(FILE);
