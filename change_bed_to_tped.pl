#!/usr/bin/perl
use warnings;
##Script to change bed files into tped (PLINK), including all the snp sites in that file.
##Author: Consuelo Quinto 
##Jan 31, 2017

##The input is the testvariant file
$input=$ARGV[0];

$out=$input.".tped";
open(OUT,">$out");

open(FILE,"$input");

while(<FILE>){
	chomp;
	@split=split(/\t/,$_);
	##Check that the 5th column in the input file has a snp
	if ($split[3] eq "snp"){
		$len=scalar(@split);
		@chr=split('chr',$split[1]); #have to print $chr[1]
		##This string has the chromosome number, a name for the site and the physical position of the SNP. The zero corresponds to the genetic position. 
		$out=$chr[1]." ".$split[0]."_".$split[2]." 0 ".$split[2]." ";
		##Grab the reference and alternative allele
		$ref_allele=$split[4];
		$seq_allele=$split[5];

		##Start reading from column 7 (8 in the file), where the genotypes start
		for($i=7;$i<$len;$i++){
			if($i==($len-1)){
				$genotype=$split[$i];
				##Change the 0 and 1's into the reference and sequenced alleles 
				$genotype=~s/0/$ref_allele/g;
				$genotype=~s/1/$seq_allele/g;

				##Make NA (missing) genotypes into 0's 
				$genotype=~s/N/0/g;

				##Get the 1st allele 
				$sub=substr($genotype,0,1);
				##Get the 2nd allele
				$sub2=substr($genotype,1,2);

				#Put the missing allele first
				if ($sub2 eq '0'){
					$out.=$sub2." ".$sub."\n";
				}
				else{
					$out.=$sub." ".$sub2."\n";
				}
			}
			else{
				$genotype=$split[$i];
				$genotype=~s/0/$ref_allele/g;
				$genotype=~s/1/$seq_allele/g;
				$genotype=~s/N/0/g;
				$sub=substr($genotype,0,1);
				$sub2=substr($genotype,1,2);
				if ($sub2 eq '0'){
					$out.=$sub2." ".$sub." ";
				}
				else{
					$out.=$sub." ".$sub2." ";
				}
			}
		}
		print OUT "$out";
	}
}
close(FILE);
close(OUT);
		
